<!-- footer -->
	<footer>
		<div class="footer-overlay">
			<div class="w3ls-footer-grids pt-sm-4 pt-3">
				<div class="container py-xl-5 py-lg-3">
					<div class="row">
						<div class="col-md-4 w3l-footer">
							<h2 class="mb-sm-3 mb-2">
								<a href="index.html" class="text-white font-italic font-weight-bold">
									<img src="images/teraCareLogo.png" alt="Tera Care Logo" class="teraCareLogo img-fluid">
								</a>
							</h2>
							<p>
								Tera Care is creating a new generation of luxury boutique staff to deliver quality service to care and residential homes in the UK. Each of our staff built around the unique needs of each individual. 
							</p>
						</div>
						<div class="col-md-4 w3l-footer my-md-0 my-4">
							<h3 class="mb-sm-3 mb-2 text-white">Address</h3>
							<ul class="list-unstyled">
								<li>
									<i class="fas fa-location-arrow float-left"></i>
									<p class="ml-4">
										<span>10 Littlemore Road SE2 9DG London</span></p>
									<div class="clearfix"></div>
								</li>
								<li class="my-3">
									<i class="fas fa-phone float-left"></i>
									<p class="ml-4">02035385978 / 07367184408</p>
									<div class="clearfix"></div>
								</li>
								<li>
									<i class="far fa-envelope-open float-left"></i>
									<a href="" class="ml-3">info@teracare.co.uk</a>
									<div class="clearfix"></div>
								</li>
							</ul>
						</div>
						<div class="col-md-4 w3l-footer">
							<h3 class="mb-sm-3 mb-2 text-white"> Quick Links </h3>
							<div class="nav-w3-l">
								<ul class="list-unstyled">
									<li>
										<a href="index.php">Home</a>
									</li>
									<li class="mt-2">
										<a href="about.php">About Us</a>
									</li>
									<li class="mt-2">
										<a href="social-care.php">Social Care</a>
									</li>
									<li class="mt-2">
										<a href="HR.php">Human Resources</a>
									</li>
									<li class="mt-2">
										<a href="contact.php">Contact Us</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div>
						<img src="images/AoHT-Member-Logo1.png" alt="AoHT-Member-Logo" class="aoht-member-logo img-fluid">
					</div>
					<div class="border-top mt-5 pt-lg-4 pt-3 pb-lg-0 pb-3 text-center">
						<p class="copy-right-grids mt-lg-1">© <?php echo date("Y"); ?> Tera Care. All Rights Reserved | Design by
							<a href="onthegotechnologies.com">OnTheGo Technologies ltd</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- //footer -->