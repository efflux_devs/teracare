<!DOCTYPE html>
<html lang="zxx">
<head>
	<title> TeraCare | About </title>
	<meta name="viewport" content="Tera Care ">
	<meta charset="UTF-8" />
	<meta name="keywords" content="Tera care, adult care, recruitment, nursing,blitz cleaning, home care service," />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>

	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/fontawesome-all.css">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	    rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	    rel="stylesheet">
</head>

<body>
	<?php
		include 'header.php';
	?>
	
	<div id="home">
		<div class="main-top py-1">
			<nav class="navbar navbar-expand-lg navbar-light fixed-navi">
				<div class="container">
					<h1>
						<a class="navbar-brand font-weight-bold font-italic" href="index.php">
							<img src="images/teraCareLogo.png" alt="Tera Care Logo" class="teraCareLogo img-fluid">
						</a>
					</h1>
					<!-- //logo -->
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
						<ul class="navbar-nav ml-lg-auto">
							<li class="nav-item  mt-lg-0 mt-3">
								<a class="nav-link" href="index.php">Home
									<span class="sr-only">(current)</span>
								</a>
							</li>
							<li class="nav-item active mx-lg-4 my-lg-0 my-3">
								<a class="nav-link" href="about.php">About Us</a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
								    aria-haspopup="true" aria-expanded="false">
									Jobs information
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown">
									<a class="dropdown-item" href="social-care.php">Social Care</a>
									<a class="dropdown-item" href="admin-clerical.php">Admin Clerical</a>
									<a class="dropdown-item" href="blitz-cleaning.php">Blitz cleaning</a>
									<a class="dropdown-item" href="HR.php">Human Resources</a>
								</div>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="contact.php">Contact Us</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</div>
	<!-- //header 2 -->

	<!-- banner 2 -->
	<div class="inner-banner-w3ls">
		<div class="container">

		</div>
	</div>
	<!-- page details -->
	<div class="breadcrumb-agile">
		<div aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="index.php">Home</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page">About Us</li>
			</ol>
		</div>
	</div>
	<!-- //page details -->

	<!-- about -->
	<section class="about py-5">
		<div class="container py-xl-5 py-lg-3">
			<div class="w3ls-titles text-center mb-md-5 mb-4 margin-bottom-rm">
				<h3 class="title align"> Our Story </h3>
			</div>

			<p class="aboutpara mx-auto align-p">
				Tera Care is a social healthcare and nursing services provider supplying high quality care, support and nursing staff to a wide range of client groups which includes young adult, elderly people, people with learning disabilities, mental health illness, dementia, people with disability and others who require support.Tera Care is creating a new generation of luxury boutique staff to deliver quality service to care and residential homes in the UK. Each of our staff built around the unique needs of each individual. 
			</p>
			</div>
		</div>
	</section>
	<!-- //about -->

	<!-- middle section -->
	<div class="w3ls-welcomee py-5">
		<div class="container py-xl-5 py-lg-3">
			<div class="row">
				<div class="col-lg-12 welcome-right mt-4">
					<h3 class="text-center head-color"> Our Core Values </h3>
					<ul class="text-center-ul">
						<li>
							To provide high quality care and support service for individuals
						</li>
						<li>
							To be outcome-focused
						</li>
						<li>
							To appreciate individual choice and preferences
						</li>
						<li>
							To treat individuals with dignity and respect
						</li>
						<li>
							To make individuals more independent and put them in control of their lives
						</li>
						<li>
							We are easily accessible by phone and email and we will get back to you immediately.
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<?php
		include 'footer.php';
	?>


	<!-- Js files -->
	<!-- JavaScript -->
	<script src="js/jquery-2.2.3.min.js"></script>
	<!-- Default-JavaScript-File -->

	<!-- banner slider -->
	<script src="js/responsiveslides.min.js"></script>
	<script>
		$(function () {
			$("#slider4").responsiveSlides({
				auto: true,
				pager: true,
				nav: true,
				speed: 1000,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});
		});
	</script>
	<!-- //banner slider -->

	<!-- fixed navigation -->
	<script src="js/fixed-nav.js"></script>
	<!-- //fixed navigation -->

	<!-- smooth scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<!-- move-top -->
	<script src="js/move-top.js"></script>
	<!-- easing -->
	<script src="js/easing.js"></script>
	<!--  necessary snippets for few javascript files -->
	<script src="js/medic.js"></script>

	<script src="js/bootstrap.js"></script>
	<!-- Necessary-JavaScript-File-For-Bootstrap -->

	<!-- //Js files -->

</body>

</html>