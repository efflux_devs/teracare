<header>
	<div class="top-bar py-3">
		<div class="container">
			<div class="row">
				<div class="col-lg-7 top-social-agile">
					<div class="row">
						<!-- social icons -->
						<ul class="col-lg-4 col-6 top-right-info text-center">
							<li>
								<a href="#">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>
							<li class="mx-3">
								<a href="#">
									<i class="fab fa-twitter"></i>
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fab fa-instagram"></i>
								</a>
							</li>
						</ul>
						<!-- //social icons -->
						<div class="col-6  pl-3 text-lg-left text-center">
							
						</div>
					</div>
				</div>
				<div class="col-lg-5 top-social-agile text-lg-right text-center">
					<div class="row">
						<div class="col-lg-7 col-6 top-w3layouts">
							<p class="text-white">
								<i class="far fa-envelope-open mr-2"></i>
								info@teracare.co.uk
							</p>
						</div>
						<div class="col-lg-5 col-6 header-w3layouts pl-4 text-lg-left">
							<p class="text-white">
								<i class="fas fa-phone mr-2"></i>02035385978</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>