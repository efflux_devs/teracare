<!DOCTYPE html>
<html lang="en">
<head>
	<title> TeraCare | Home </title>
	<meta name="viewport" content="Tera Care ">
	<meta charset="UTF-8" />
	<meta name="keywords" content="Tera care, adult care, recruitment, nursing,blitz cleaning, home care service," />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/fontawesome-all.css">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	    rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"rel="stylesheet">
</head>

<body>
	<?php
		include 'header.php';
	?>

	<div id="home">
		<div class="main-top py-1">
			<nav class="navbar navbar-expand-lg navbar-light fixed-navi">
				<div class="container">
					<!-- logo -->
					<h1>
						<a class="navbar-brand font-weight-bold font-italic" href="index.php">
							<img src="images/teraCareLogo.png" alt="Tera Care Logo" class="teraCareLogo img-fluid">
						</a>
					</h1>
					<!-- //logo -->
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
						<ul class="navbar-nav ml-lg-auto">
							<li class="nav-item active mt-lg-0 mt-3">
								<a class="nav-link" href="index.php">Home
									<span class="sr-only">(current)</span>
								</a>
							</li>
							<li class="nav-item mx-lg-4 my-lg-0 my-3">
								<a class="nav-link" href="about.php">About Us</a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
								    aria-haspopup="true" aria-expanded="false">
									Jobs information
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown">
									<a class="dropdown-item" href="social-care.php">Social Care</a>
									<a class="dropdown-item" href="admin-clerical.php">Admin Clerical</a>
									<a class="dropdown-item" href="blitz-cleaning.php">Blitz cleaning</a>
									<a class="dropdown-item" href="HR.php">Human Resources</a>
								</div>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="contact.php">Contact Us</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</div>
	
	<div class="slider">
		<div class="callbacks_container">
			<ul class="rslides callbacks callbacks1" id="slider4">
				<li>
					<div class="banner-top1">
						<div class="banner-info_agile_w3ls">
							<div class="container">
								<h3>
									Social Care
								</h3>
								
								<p class="mt-3 mb-md-5 mb-3">
									If you need practical help and support because of your illness, disability or age, then social care services can help you. 
								</p>
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="banner-top2">
						<div class="banner-info_agile_w3ls">
							<div class="container">
								<h3>
									<h3>
										Blitz Cleaning
									</h3>
								</h3>
								
								<p class="mt-3 mb-md-5 mb-3">
									TeraCare understands the importance of your company’s image and office appearance and we believe that our trained professionals can provide you with the highest level of performance on a consistent basis.
								</p>
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="banner-top3">
						<div class="banner-info_agile_w3ls">
							<div class="container">
								<h3>
									Admin Clerical
								</h3>
								<p class="mt-3 mb-md-5 mb-3">
									Clerical work typically refers to a variety of office and administrative support duties. If you're interested in a career in clerical work, read on to learn more about the nature of the profession and the variety of occupations available
								</p>
							</div>
						</div>
					</div>
				</li>
				<li>
					<div class="banner-top4">
						<div class="banner-info_agile_w3ls">
							<div class="container">
								<h3>
									Human Resources (HR)
								</h3>
								
								<p class="mt-3 mb-md-5 mb-3">
									The larger the company, the greater the HR need. At the HR Company we have the capacity to provide you with the level of HR support you deserve. 
								</p>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="w3ls-welcome py-5">
		<div class="container py-xl-5 py-lg-3">
			<div class="row">
				<div class="col-lg-5 welcome-right">
					<img src="images/side-banner.jpg" alt=" " class="img-fluid">
				</div>
				<div class="col-lg-7 welcome-left">
					<h3 class="homepage-h3">
						Here are the key aspects of our service and working ethos that makes us the first choice for employers seeking quality staff:
					</h3>
					<ul class="key-ul">
						<li>We operate 365 days a year, 24 hours a day</li>
						<li>Rapid response to your staffing needs</li>
						<li>DBS checks carried out</li>
						<li>Skills test and appraisals</li>
						<li>Risk and quality assessments</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="why-choose-agile pt-5" id="services">
		<div class="container pt-xl-5 pt-lg-3">
			<div class="w3ls-titles text-center mb-5">
				<h3 class="title">Our Best Services</h3>
				<p class="mt-2">
					Look at what we do below:
				</p>
			</div>
			<div class="row why-choose-agile-grids-top">
				<div class="col-lg-4 agileits-w3layouts-grid">
					<div class="row wthree_agile_us">
						<div class="col-3 agile-why-text p-0 text-right">
							<div class="wthree_features_grid">
								<i class="fas fa-user-md"></i>
							</div>
						</div>
						<div class="col-9 agile-why-text-2">
							<h4 class="text-dark font-weight-bold mb-3">Adult Care</h4>
							<p>
								Getting old doesn’t have to mean giving up fun and social activity. Just ask any of the We Care Adult Care, Inc.
							</p>
						</div>
					</div>
					<div class="row wthree_agile_us my-5">
						<div class="col-3 agile-why-text p-0 text-right">
							<div class="wthree_features_grid">
								<i class="fas fa-syringe"></i>
							</div>
						</div>
						<div class="col-9 agile-why-text-2">
							<h4 class="text-dark font-weight-bold mb-3"> Recruitment service </h4>
							<p>Recruitment refers to the overall process of attracting, shortlisting, selecting and appointing suitable candidates for jobs (either permanent or temporary) within an organization</p>
						</div>
					</div>
					<div class="row wthree_agile_us">
						<div class="col-3 agile-why-text p-0 text-right">
							<div class="wthree_features_grid">
								<i class="fab fa-medrt"></i>
							</div>
						</div>
						<div class="col-9 agile-why-text-2">
							<h4 class="text-dark font-weight-bold mb-3">Blitz Cleaning</h4>
							<p>
								You know how to do your job exceptionally well – why spend your time cleaning?
							</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 agileits-w3layouts-grid img text-center">
					<img src="images/b3.png" alt=" " class="img-fluid" />
				</div>
				<div class="col-lg-4 agileits-w3layouts-grid">
					<div class="row wthree_agile_us">
						<div class="col-9 agile-why-text-2">
							<h4 class="text-dark font-weight-bold mb-3">Social Care</h4>
							<p>
								If you need practical help and support because of your illness, disability or age, then social care services can help you. 
							</p>
						</div>
						<div class="col-3 agile-why-text p-0">
							<div class="wthree_features_grid">
								<i class="fas fa-medkit"></i>
							</div>
						</div>
					</div>
					<div class="row wthree_agile_us my-5">
						<div class="col-9 agile-why-text-2">
							<h4 class="text-dark font-weight-bold mb-3">Human Resources</h4>
							<p>
								The larger the company, the greater the HR need. At the HR Company we have the capacity to provide you with the level of HR support you deserve. 
							</p>
						</div>
						<div class="col-3 agile-why-text p-0">
							<div class="wthree_features_grid">
								<i class="fas fa-wheelchair"></i>
							</div>
						</div>
					</div>
					<div class="row wthree_agile_us">
						<div class="col-9 agile-why-text-2">
							<h4 class="text-dark font-weight-bold mb-3">Training</h4>
							<p> Tera Care Recruitment offer employers recruitment and training facilities. </p>
						</div>
						<div class="col-3 agile-why-text p-0">
							<div class="wthree_features_grid">
								<i class="fas fa-hospital"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="py-5" id="blog">
		<div class="container">
			<h3 class="title align">Why Choose Us</h3>
			<ul class="why-ul">
				<li>
					Your choices, beliefs & goals come first for us
				</li>
				<li>
					We are a dedicated caring team
				</li>
				<li>
					Our support workers are chosen on the basis of their compassionate and caring attitude
				</li>
				<li>
					We regularly visit to see if you are satisfied with our support or if we need to improve!
				</li>
			</ul>
		</div>
	</div>
	
	<?php
		include 'footer.php';
	?>

	<!-- Js files -->
	<!-- JavaScript -->
	<script src="js/jquery-2.2.3.min.js"></script>
	<!-- Default-JavaScript-File -->

	<!-- banner slider -->
	<script src="js/responsiveslides.min.js"></script>

	<script>
		$(function () {
			$("#slider4").responsiveSlides({
				auto: true,
				pager: true,
				nav: true,
				speed: 1000,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});
		});
	</script>
	<!-- //banner slider -->

	<!-- fixed navigation -->
	<script src="js/fixed-nav.js"></script>
	<!-- //fixed navigation -->

	<!-- smooth scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<!-- move-top -->
	<script src="js/move-top.js"></script>
	<!-- easing -->
	<script src="js/easing.js"></script>
	<!--  necessary snippets for few javascript files -->
	<script src="js/medic.js"></script>

	<script src="js/bootstrap.js"></script>
	<!-- Necessary-JavaScript-File-For-Bootstrap -->

	<!-- //Js files -->

</body>

</html>