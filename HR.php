<!DOCTYPE html>
<html lang="eng">
<head>
	<title> Tera Care | Human Resource </title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8" />
	<meta name="keywords" content="Medic Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/fontawesome-all.css">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	    rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	    rel="stylesheet">
</head>

<body>
	<?php
		include 'header.php';
	?>
	
	<div id="home">
		<div class="main-top py-1">
			<nav class="navbar navbar-expand-lg navbar-light fixed-navi">
				<div class="container">
					<h1>
						<a class="navbar-brand font-weight-bold font-italic" href="index.php">
							<img src="images/teraCareLogo.png" alt="Tera Care Logo" class="teraCareLogo img-fluid">
						</a>
					</h1>
					<!-- //logo -->
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
						<ul class="navbar-nav ml-lg-auto">
							<li class="nav-item  mt-lg-0 mt-3">
								<a class="nav-link" href="index.php">Home
									<span class="sr-only">(current)</span>
								</a>
							</li>
							<li class="nav-item mx-lg-4 my-lg-0 my-3">
								<a class="nav-link" href="about.php">About Us</a>
							</li>
							<li class="nav-item active dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
								    aria-haspopup="true" aria-expanded="false">
									Jobs information
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdown">
									<a class="dropdown-item" href="social-care.php">Social Care</a>
									<a class="dropdown-item" href="admin-clerical.php">Admin Clerical</a>
									<a class="dropdown-item" href="blitz-cleaning.php">Blitz cleaning</a>
									<a class="dropdown-item" href="HR.php">Human Resources</a>
								</div>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="contact.php">Contact Us</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</div>
	
	<div class="inner-banner-w3ls-HR">
		<div class="container">

		</div>
	</div>

	<div class="breadcrumb-agile">
		<div aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="index.php">Home</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page"> Human Resources </li>
			</ol>
		</div>
	</div>
	
	<div class="container">
		<div class="row inner_sec_info">
			<div class="col-lg-8 single-left">
				<h3 class="jobs-h3">Human Resources</h3>
				<div class="right-banner-div-HR">
					
				</div>
				<div class="content-div">
					<p>
						Human Resources role within Local authorities throughout London. Great team who are both professional and fun!
						This Human Resources role will be involved in the complete “employee life cycle” from recruitment, on boarding including contracts to exit interviews and there will also be some contact with payroll and benefits, so any administration needed along this journey will be part of this true generalist remit. There will also be the opportunity to get involved in HR Projects. This is a fantastic entry level role. 
					</p>
					<h3>CANDIDATE</h3>
					<p>
						Will have some HR administration experience gained in a local authority. Applicants must be eager to learn, have the appetite to take on projects and be enthusiastic as well as having a “can-do” attitude, with some solid administration experience, preferably gained in an HR department, therefore great developmental opportunities. <br> <br>
						To find out more information about our Health Care Funding Supporting Services in the London area and surrounding counties, why not get in touch today! Call us on 02035385978/ 07367184408 or fill in our <a href="contact.php">contact form</a>
					</p>
				</div>
			</div>
		
			<div class="col-lg-4 event-right mt-lg-0 mt-sm-5 mt-4 margin-top-categories">
				<div class="event-right1">
					<div class="categories my-4 p-4 border">
						<h3 class="blog-title text-dark">Search by Categories</h3>
						<ul>
							<li class="mt-3">
								<i class="fas fa-check mr-2"></i>
								<a href="social-care.php">Social Care</a>
							</li>
							<li class="mt-3">
								<i class="fas fa-check mr-2"></i>
								<a href="blitz-cleaning.php">Blitz cleaning</a>
							</li>
							<li class="mt-3">
								<i class="fas fa-check mr-2"></i>
								<a href="admin-clerical.php">Admin Clerical</a>
							</li>
							<li class="mt-3">
								<i class="fas fa-check mr-2"></i>
								<a href="HR.php">Human Resources</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- //single -->

	<?php
		include 'footer.php';
	?>


	<!-- Js files -->
	<!-- JavaScript -->
	<script src="js/jquery-2.2.3.min.js"></script>
	<!-- Default-JavaScript-File -->

	<!-- banner slider -->
	<script src="js/responsiveslides.min.js"></script>
	<script>
		$(function () {
			$("#slider4").responsiveSlides({
				auto: true,
				pager: true,
				nav: true,
				speed: 1000,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});
		});
	</script>
	<!-- //banner slider -->

	<!-- fixed navigation -->
	<script src="js/fixed-nav.js"></script>
	<!-- //fixed navigation -->

	<!-- smooth scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<!-- move-top -->
	<script src="js/move-top.js"></script>
	<!-- easing -->
	<script src="js/easing.js"></script>
	<!--  necessary snippets for few javascript files -->
	<script src="js/medic.js"></script>

	<script src="js/bootstrap.js"></script>
	<!-- Necessary-JavaScript-File-For-Bootstrap -->

	<!-- //Js files -->

</body>

</html>